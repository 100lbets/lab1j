import java.util.Scanner;

public class lab1 {
  // Программа спрашивает какое действие выполнить
  public static void main(String[] args) {
    System.out.println("Что сделать:");
    System.out.println("1 Поиск простых чисел");
    System.out.println("2 Проверка на палиндром");
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt(); // Считываем выбор пользователя
    while (i != 1 && i != 2) { // Спрашиваем до тех пор пока не получим 1 или 2
      System.out.println("Выберите 1 или 2");
      i = sc.nextInt();
    }
    if (i == 1) prostCh();
    if (i == 2) palindrom();
  }

  // Считывает с клавиатуры число и выводит на экран все простые числа от 1 до считанного числа
  private static void prostCh() {
    int n = readInt();
    while (n <= 1) { // Продолжаем спрашивать пользователя пока он не введет число больше 1
      System.out.println("Введите число больше 1");
      n = readInt();
    }
    System.out.println("1"); // Сразу выводим 1, потому что оно является простым числом
    for (int i=2; i <= n; i++) { // Теперь проверяем для всех чисел от 2 до n
      if (isSimple(i)) { // Если число простое,
        System.out.println(i); // то выводим его
      }
    }
  }

  // Считывает с клавиватуры строку и проверяет является ли она палиндромом
  private static void palindrom() {
    String str = readString(); // Считываем строку
    boolean p = isPalindrome(str); // и проверяем является ли она палиндромом
    if (p) {
      System.out.println("Палиндром");
    }
    else {
      System.out.println("Не палидром");
    }
  }

  // Считывает число
  private static int readInt() {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    return n;
  }

  // Считывает строку
  private static String readString() {
    Scanner sc = new Scanner(System.in);
    String str = sc.nextLine();
    return str;
  }

  // Проверяет на простое число
  private static boolean isSimple(int n) {
    for (int i=2; i < n; i++) { // Проходим по всем числам от 2 до n
      if (n % i == 0) { // если n делится на какое-то число кроме 1 и себя, то оно не простое 
        return false;
      }
    }
    return true; // иначе оно простое
  }

  // Проверяет на палиндром
  private static boolean isPalindrome(String str) {
    int c;// находим индекс центра строки
    if (str.length() % 2 == 0) { // если длина строки четная
      c = str.length() / 2; // то центром будет символ после середины
    } else {
      c = (str.length() - 1) / 2; // иначе берем символ по центру
    }
    for (int i = 0; i <= c; i++) { // Проходимся по всем индексам от 0 до центра
      if (str.charAt(i) != str.charAt(str.length() - 1 - i)) { // если символ в какой-либо позиции
        return false; // в левой половине не равен симметричному символу, то это не палиндром
      }
    }
    return true; // иначе это палиндром
  }

}
